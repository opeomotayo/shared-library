#!/usr/bin/env groovy

def call(Map stageParams) {
    
    def safeParams = stageParams ?: [:]
    def app_name   = safeParams['app_name'] ?: 'helloworld-app'
    def app_version   = safeParams['app_version'] ?: 'latest'
    def docker_dir = safeParams['docker_dir'] ?: 'helloworld-alpine-base'
    

    if (!app_name) {
        error 'app_name is a required input.'
    }
    
    withCredentials([string(credentialsId: 'snyk-api-key', variable: 'SNYK_TOKEN')]) {
            sh """
            cd /var/jenkins_home/tools/io.snyk.jenkins.tools.SnykInstallation/snyk_latest
            ./snyk-linux auth ${SNYK_TOKEN}
            ./snyk-linux test --docker debian
           
              """  
            }
}
