#!/usr/bin/env groovy

def call(Map stageParams) {
    
    def IMAGE_VERSION = "latest"
    def safeParams = stageParams ?: [:]
    def app_name   = safeParams['app_name']
    def snyk_token   = safeParams['snyk_token']
    def app_version   = safeParams['app_version'] ?: 'latest'
    def docker_dir = safeParams['docker_dir'] ?: ''
    

    if (!app_name) {
        error 'app_name is a required input.'
    }
    
    withCredentials([string(credentialsId: 'snyk-token', variable: 'SNYK_TOKEN')]) {
     sh """
        curl -Lo ./snyk https://github.com/snyk/snyk/releases/download/v1.210.0/snyk-linux
        snyk --version
        chmod -R +x ./snyk
        snyk auth ${SNYK_TOKEN}
        docker build -t $app_name:$app_version ./$docker_dir 
        snyk test --severity-threshold=high --docker $app_name:$app_version --file=./$docker_dir/Dockerfile --fail-on=upgradable -show-vulnerable-paths=false --exclude-base-image-vulns
        #if it fails use sudo
        snyk monitor --docker $app_name:$app_version --file=./$docker_dir/Dockerfile
        docker image prune -a --filter "until=12h"
        """  
    }
}
